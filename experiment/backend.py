#!/usr/bin/env python3
import os
import re
import sys
import json
import logging
import subprocess
from datetime import datetime
from flask import Flask, Response


app = Flask(__name__)
logging.root.setLevel(logging.INFO)
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
next_vnc = 0


@app.route("/new")
def new_vm():
    global next_vnc
    log.info('Creating new VM')
    vm_id = next_vnc
    log.info('Copying c7 template')
    hdd_path = 'store/hdd/%s.qcow2' % vm_id
    subprocess.call(['cp', 'store/template/c7-v1-updated.qcow2', hdd_path])
    log.info('Forking')
    pid = os.fork()
    if pid == 0:
        # os.chdir('/')
        os.setsid()
        os.umask(0)
        os.execvp(
            '/usr/bin/qemu-system-x86_64',
            [
                'qemu-system-x86_64',
                '-enable-kvm',
                '-smp', '2',
                '-m', '2048',
                '-vnc', ':%s' % next_vnc,
                '-drive',
                'file=%s,if=virtio,media=disk,cache=none,format=qcow2' %
                hdd_path])
    log.info('Spawned PID %s', pid)
    next_vnc += 1
    return Response(
        response=json.dumps({'vnc_port': next_vnc - 1, 'pid': pid}),
        status=200,
        headers={'Access-Control-Allow-Origin': '*'},
        content_type='application/json')


def _list_vms():
    rv = subprocess.run(['pgrep', '-a', 'qemu-system'], stdout=subprocess.PIPE)
    processes = {}
    for line in rv.stdout.decode().split('\n'):
        if line:
            pid, cmd = line.split(' ', 1)
            processes[pid] = cmd
    return processes


@app.route("/list")
def list_vms():
    processes = _list_vms()
    return Response(
        response=json.dumps(processes),
        status=200,
        headers={'Access-Control-Allow-Origin': '*'},
        content_type='application/json')


def _parse_hdd_path(proc):
    m = re.search(r'file=store/hdd/[^.]+\.qcow2', proc)
    if m:
        return m.group(0).split('=')[1]


@app.route("/destroy/<pid>")
def destroy_vm(pid):
    msg = None
    vms = _list_vms()
    if pid not in vms:
        msg = "No such vm"
        status = 1
    else:
        proc = vms[pid]
        rv = subprocess.run(
            ['kill', pid], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if rv.returncode == 0:
            hdd_path = _parse_hdd_path(proc)
            print(hdd_path)
            if hdd_path:
                rv = subprocess.run(
                    ['rm', hdd_path],
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                if rv.returncode == 0:
                    status = 0
                    msg = "destroyed"
                else:
                    msg = rv.stdout.decode() + " " + rv.stderr.decode()
                    status = rv.returncode
            else:
                msg = "Failed to delete hdd"
                status = 1
        else:
            msg = rv.stdout.decode() + " " + rv.stderr.decode()
            status = rv.returncode
    return Response(
        response=json.dumps({"status": status, "message": msg}),
        status=200,
        headers={'Access-Control-Allow-Origin': '*'},
        content_type='application/json')


app.run()
