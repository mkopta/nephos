create table "task" (
	"id" serial primary key,
	"parent" integer,
	"uid" integer not null,
	"state" task_state not null,
	"name" varchar(40) not null,
	"args" varchar(256),
	"created" timestamp not null default current_timestamp,
	"updated" timestamp not null default current_timestamp,
	foreign key ("uid") references "user"("id"),
	foreign key ("parent") references "task"("id")
);
