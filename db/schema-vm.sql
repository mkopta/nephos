create table "vm" (
	"id" serial primary key,
	"uid" integer not null,
	"name" varchar(40) not null,
	"cpu" integer not null,
	"ram" integer not null,
	foreign key ("uid") references "user"("id")
);
