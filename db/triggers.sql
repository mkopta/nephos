create trigger on_update_task
	before update on "task"
	for each row
		execute procedure updated();
