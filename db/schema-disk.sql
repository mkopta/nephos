create table "disk" (
	"id" serial primary key,
	"uid" integer not null,
	"size" integer not null,
	"desc" varchar(128),
	foreign key ("uid") references "user"("id")
);
