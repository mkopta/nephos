create or replace function updated()
	returns trigger as '
	begin
	new.updated = now();
	return new;
	end;
	' language 'plpgsql';
