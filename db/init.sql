\i shred.sql

\i types.sql
\i functions.sql

\i schema-user.sql
\i schema-vm.sql
\i schema-disk.sql
\i schema-task.sql

\i triggers.sql
