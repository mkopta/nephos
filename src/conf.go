package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
)

func conf_load() {
	json_data := conf_read("nephos.conf")
	conf := conf_parse(json_data)
	err := conf_check(conf)
	errchk(err)
	global_conf = conf
}

func conf_read(filename string) []byte {
	json_data, err := ioutil.ReadFile(filename)
	errchk(err)
	return json_data
}

func conf_parse(json_data []byte) *conf_t {
	var c conf_t
	err := json.Unmarshal(json_data, &c)
	errchk(err)
	return &c
}

func conf_check(c *conf_t) error {
	var e error
	switch {
	case c == nil:
		e = errors.New("Configuration not loaded")
	case c.KVM_sockets_dir == nil:
		e = errors.New("Missing 'kvm_sockets_dir' configuration value")
	case c.KVM_pids_dir == nil:
		e = errors.New("Missing 'kvm_pids_dir' configuration value")
	case c.Disks_dir == nil:
		e = errors.New("Missing 'disks_dir' configuration value")
	case c.DB_user == nil:
		e = errors.New("Missing 'db_user' configuration value")
	case c.DB_pass == nil:
		e = errors.New("Missing 'db_pass' configuration value")
	}
	return e
}
