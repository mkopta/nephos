package main

func cleanup() {
	db_close() // if open
}

func main() {
	conf_load()
	cli()
	cleanup()
}
