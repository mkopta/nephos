package main

import (
	"database/sql"
	_ "github.com/bmizerany/pq"
)

func db() *sql.DB {
	if global_db_handle == nil {
		db_init(*global_conf.DB_user, *global_conf.DB_pass)
	}
	return global_db_handle
}

func db_init(user, dbname string) {
	var connection_string, driver_name string
	driver_name = "postgres"
	connection_string = "user=" + user
	connection_string += " dbname=" + dbname
	connection_string += " sslmode=disable"
	db_handle, err := sql.Open(driver_name, connection_string)
	errchk(err)
	global_db_handle = db_handle
}

func db_close() {
	if global_db_handle != nil {
		err := db().Close()
		errchk(err)
	}
}
