package main

import (
	"fmt"
	"os"
	"strconv"
)

func cli() {
	if len(os.Args) == 1 {
		cli_usage()
		return
	}
	switch os.Args[1] {
	case "vm.list":
		cli_vm_list()
	case "vm.create":
		cli_vm_create()
	case "vm.start":
		cli_vm_start()
	case "vm.stop":
		cli_vm_stop()
	case "vm.freeze":
		cli_vm_freeze()
	case "vm.thaw":
		cli_vm_thaw()
	case "vm.kill":
		cli_vm_kill()
	case "vm.reset":
		cli_vm_reset()
	case "vm.delete":
		cli_vm_delete()
	case "disk.list":
		cli_disk_list()
	case "disk.create":
		cli_disk_create()
	case "disk.delete":
		cli_disk_delete()
	case "help":
		cli_help()
	default:
		cli_bad_usage()
	}
}

func cli_usage() {
	fmt.Printf("Usage: %s <command> [<command args>]\n", os.Args[0])
	fmt.Printf("Use '%s help' to see all commands\n", os.Args[0])
}

func cli_bad_usage() {
	fmt.Println("Bad usage")
	cli_usage()
}

func cli_help() {
	if len(os.Args) == 2 {
		cli_help_overview()
		return
	} else if len(os.Args) > 3 {
		cli_bad_usage()
		return
	}
	switch os.Args[2] {
	case "help":
		cli_help_help()
	case "vm.list":
		cli_help_vm_list()
	case "vm.create":
		cli_help_vm_create()
	case "vm.start":
		cli_help_vm_start()
	case "vm.stop":
		cli_help_vm_stop()
	case "vm.kill":
		cli_help_vm_kill()
	case "vm.reset":
		cli_help_vm_reset()
	case "vm.freeze":
		cli_help_vm_freeze()
	case "vm.thaw":
		cli_help_vm_thaw()
	case "vm.delete":
		cli_help_vm_delete()
	case "disk.list":
		cli_help_disk_list()
	case "disk.create":
		cli_help_disk_create()
	case "disk.delete":
		cli_help_disk_delete()
	default:
		cli_help_unknown()
	}
}

func cli_help_overview() {
	fmt.Println("help           show overview of commands")
	fmt.Println("help <command> show help for given command")
	fmt.Println("vm.list        list all known vms")
	fmt.Println("vm.create      create new vm")
	fmt.Println("vm.start       boot up already defined vm")
	fmt.Println("vm.stop        politely (ACPI) ask vm to shutdown")
	fmt.Println("vm.kill        terminate vm at once")
	fmt.Println("vm.reset       forcefully reset vm at once")
	fmt.Println("vm.freeze      temporarily deny CPU to vm")
	fmt.Println("vm.thaw        resume freezed vm")
	fmt.Println("vm.delete      delete vm")
	fmt.Println("disk.list      list all known virtual disks")
	fmt.Println("disk.create    create new virtual disk")
	fmt.Println("disk.delete    delete virtual disk")
}

func cli_help_help() {
	fmt.Printf("Usage: %s help <command>\n", os.Args[0])
	fmt.Println("Show help for given command.")
}

func cli_help_vm_list() {
	fmt.Printf("Usage: %s vm.list\n", os.Args[0])
	fmt.Println("List all known virtual machines.")
}

func cli_help_vm_create() {
	fmt.Printf("Usage: %s vm.create <name> <cpu#> <ram>\n", os.Args[0])
	fmt.Println("Create new virtual machine.")
	fmt.Println("  name  name and hostname of the virtual machine")
	fmt.Println("  cpu#  cpu count")
	fmt.Println("  ram   ram size in MiB")
}

func cli_help_vm_start() {
	fmt.Printf("Usage: %s vm.start <id>\n", os.Args[0])
	fmt.Println("Boot up already defined vm refered by id.")
}

func cli_help_vm_stop() {
	fmt.Printf("Usage: %s vm.stop <id>\n", os.Args[0])
	fmt.Println("Politely (ACPI) ask vm refered by id to shutdown.")
}

func cli_help_vm_kill() {
	fmt.Printf("Usage: %s vm.kill <id>\n", os.Args[0])
	fmt.Println("Forcefuly stop running virtual machine refered by id.")
}

func cli_help_vm_reset() {
	fmt.Printf("Usage: %s vm.reset <id>\n", os.Args[0])
	fmt.Println("Forcefuly reset vm refered by id.")
}

func cli_help_vm_freeze() {
	fmt.Printf("Usage: %s vm.freeze <id>\n", os.Args[0])
	fmt.Println("Temporarily deny CPU to vm refered by id.")
}

func cli_help_vm_thaw() {
	fmt.Printf("Usage: %s vm.thaw <id>\n", os.Args[0])
	fmt.Println("Resume freezed vm refered by id.")
}

func cli_help_vm_delete() {
	fmt.Printf("Usage: %s vm.delete <id>\n", os.Args[0])
	fmt.Println("Delete virtual machine refered by id.")
}

func cli_help_disk_list() {
	fmt.Printf("Usage: %s disk.list\n", os.Args[0])
	fmt.Println("List all known virtual disks.")
}

func cli_help_disk_create() {
	fmt.Printf("Usage: %s disk.create <size> <desc>\n", os.Args[0])
	fmt.Println("Create new virtual disk.")
	fmt.Println("  size  in GiB")
	fmt.Println("  desc  description of the disk")
}

func cli_help_disk_delete() {
	fmt.Printf("Usage: %s disk.delete <id>\n", os.Args[0])
	fmt.Println("Delete virtual disk refered by id.")
}

func cli_help_unknown() {
	fmt.Printf("Unknown command '%s'\n", os.Args[2])
}

func cli_vm_list() {
	if len(os.Args) > 2 {
		cli_bad_usage()
		return
	}
	vms := c_vm_list()
	if vms == nil {
		fmt.Println("There are no virtual machines")
	} else {
		fmt.Printf("id\tuid\tname\tcpu#\tram\n")
		for i := 0; i < len(vms); i++ {
			vm := vms[i]
			fmt.Printf("%d\t%d\t%s\t%d\t%d\n",
				vm.id, vm.uid, vm.name, vm.cpu, vm.ram)
		}
	}
}

func cli_vm_create() {
	if len(os.Args) != 5 {
		cli_bad_usage()
		return
	}
	name := os.Args[2]
	cpu, err := strconv.Atoi(os.Args[3])
	errchk(err)
	ram, err := strconv.Atoi(os.Args[4])
	errchk(err)
	c_vm_create(name, cpu, ram)
}

func cli_vm_start() {
	cli_basic_cmd(c_vm_start)
}

func cli_vm_stop() {
	cli_basic_cmd(c_vm_stop)
}

func cli_vm_kill() {
	cli_basic_cmd(c_vm_kill)
}

func cli_vm_reset() {
	cli_basic_cmd(c_vm_reset)
}

func cli_vm_freeze() {
	cli_basic_cmd(c_vm_freeze)
}

func cli_vm_thaw() {
	cli_basic_cmd(c_vm_thaw)
}

func cli_vm_delete() {
	cli_basic_cmd(c_vm_delete)
}

func cli_disk_list() {
	if len(os.Args) > 2 {
		cli_bad_usage()
		return
	}
	disks := c_disk_list()
	if disks == nil {
		fmt.Println("There are no virtual disks")
	} else {
		fmt.Printf("id\tuid\tsize\tdesc\n")
		for i := 0; i < len(disks); i++ {
			disk := disks[i]
			fmt.Printf("%d\t%d\t%d\t%s\n",
				disk.id, disk.uid, disk.size, disk.desc)
		}
	}
}

func cli_disk_create() {
	if len(os.Args) != 4 {
		cli_bad_usage()
		return
	}
	size, err := strconv.Atoi(os.Args[2])
	errchk(err)
	desc := os.Args[3]
	c_disk_create(size, desc)
}

func cli_disk_delete() {
	cli_basic_cmd(c_disk_delete)
}

func cli_basic_cmd(command func(id int)) {
	if len(os.Args) != 3 {
		cli_bad_usage()
		return
	}
	id, err := strconv.Atoi(os.Args[2])
	errchk(err)
	command(id)
}
