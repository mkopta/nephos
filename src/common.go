package main

import (
	"log"
	"fmt"
)

func errchk(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func path_vm_mon_sock(vm_id int) string {
	return fmt.Sprintf(
		"%s/%d.sock", *global_conf.KVM_sockets_dir, vm_id)
}

func path_vm_pidfile(vm_id int) string {
	return fmt.Sprintf(
		"%s/%d.pid", *global_conf.KVM_pids_dir, vm_id)
}

func path_disk(id int) string {
	return fmt.Sprintf(
		"%s/%d.qcow2", *global_conf.Disks_dir, id)
}
