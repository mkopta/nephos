package main

import (
	"fmt"
	"net"
	"os/exec"
	"strconv"
)

func vm_create(id int, name string, cpu, ram int) {
	chardev_mon_socket := fmt.Sprintf(
		"socket,id=charmonitor,path=%s,server,nowait",
		path_vm_mon_sock(id))
	mon := "chardev=charmonitor,id=monitor,mode=readline"
	cmd := exec.Command(
		"kvm",
		"-enable-kvm",
		"-name", name,
		"-smp", strconv.Itoa(cpu),
		"-m", strconv.Itoa(ram),
		"-vnc", ":0",
		"-pidfile", path_vm_pidfile(id),
		"-chardev", chardev_mon_socket,
		"-mon", mon)
	err := cmd.Start()
	errchk(err)
}

func vm_kill(id int) {
	vm_command(id, "q")
}

func vm_stop(id int) {
	vm_command(id, "system_powerdown")
}

func vm_freeze(id int) {
	vm_command(id, "stop")
}

func vm_thaw(id int) {
	vm_command(id, "c")
}

func vm_reset(id int) {
	vm_command(id, "system_reset")
}

func vm_command(id int, cmd string) {
	c, err := net.Dial("unix", path_vm_mon_sock(id))
	errchk(err)
	_, err = c.Write([]byte(cmd+"\n"))
	errchk(err)
}
