package main

import (
	"fmt"
	"os"
	"os/exec"
)

func disk_create(id, size int) {
	cmd := exec.Command(
		"qemu-img",
		"create",
		"-f", "qcow2",
		path_disk(id),
		fmt.Sprintf("%dG", size))
	err := cmd.Run()
	errchk(err)
}

func disk_delete(id int) {
	err := os.Remove(path_disk(id))
	errchk(err)
}
