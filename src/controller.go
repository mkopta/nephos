package main

func c_vm_list() vms_t {
	return db_vm_get_all()
}

func c_vm_create(name string, cpu, ram int) {
	id := db_vm_create(1, name, cpu, ram)
	vm_create(id, name, cpu, ram)
}

func c_vm_start(id int) {
	vm := db_vm_get_by_id(id)
	vm_create(vm.id, vm.name, vm.cpu, vm.ram)
}

func c_vm_stop(id int) {
	vm_stop(id)
}

func c_vm_kill(id int) {
	vm_kill(id)
}

func c_vm_reset(id int) {
	vm_reset(id)
}

func c_vm_freeze(id int) {
	vm_freeze(id)
}

func c_vm_thaw(id int) {
	vm_thaw(id)
}

func c_vm_delete(id int) {
	db_vm_delete(id)
}

func c_disk_create(size int, desc string) {
	id := db_disk_create(1, size, desc)
	disk_create(id, size)
}

func c_disk_delete(id int) {
	db_disk_delete(id)
	disk_delete(id)
}

func c_disk_list() []*disk_t {
	return db_disk_get_all()
}
