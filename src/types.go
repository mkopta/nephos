package main

type vm_t struct {
	id   int
	uid  int
	name string
	cpu  int
	ram  int
}

type vms_t []*vm_t

type disk_t struct {
	id   int
	uid  int
	size int
	desc string
}

type conf_t struct {
	KVM_sockets_dir *string
	KVM_pids_dir    *string
	Disks_dir       *string
	DB_user         *string
	DB_pass         *string
}

type task_t struct {
	id      int
	parent  int
	uid     int
	state   string
	name    string
	args    string
	created int
	updated int
}
