package main

import "fmt"

func db_disk_get_all() []*disk_t {
	rows, err := db().Query(`select * from "disk"`)
	errchk(err)
	disks := make([]*disk_t, 0)
	for rows.Next() {
		disk := new(disk_t)
		err = rows.Scan(&disk.id, &disk.uid, &disk.size, &disk.desc)
		errchk(err)
		disks = append(disks, disk)
	}
	err = rows.Err()
	errchk(err)
	return disks
}

func db_disk_get_by_id(id int) disk_t {
	qf := `select * from "disk" where "id" = %d`
	query := fmt.Sprintf(qf, id)
	row := db().QueryRow(query)
	var disk disk_t
	err := row.Scan(&disk.id, &disk.uid, &disk.size, &disk.desc)
	errchk(err)
	return disk
}

func db_disk_create(uid, size int, desc string) int {
	var id int
	qf := `insert into "disk"
		("uid", "size", "desc")
		values (%d, %d, '%s')
		returning "id"`
	query := fmt.Sprintf(qf, uid, size, desc)
	row := db().QueryRow(query)
	err := row.Scan(&id)
	errchk(err)
	return id
}

func db_disk_delete(id int) {
	query_fmt := `delete from "disk" where "id" = %d`
	query := fmt.Sprintf(query_fmt, id)
	_, err := db().Query(query)
	errchk(err)
}
