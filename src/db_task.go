package main

import "fmt"

func db_task_create(parent, uid int, state, name, args string) int {
	var id int
	qf := `insert into "task"
		("parent", "uid", "state", "name", "args")
		values (%d, %d, '%s', '%s', '%s')
		returning "id"`
	query := fmt.Sprintf(qf, parent, uid, state, name, args)
	row := db().QueryRow(query)
	err := row.Scan(&id)
	errchk(err)
	return id
}

func db_task_create_meta(uid int, name string) int {
	var id int
	qf := `insert into "task"
		("uid", "state", "name")
		value (%d, '%s', '%s')
		returning "id"`
	query := fmt.Sprintf(qf, uid, TASK_STATE_META, name)
	row := db().QueryRow(query)
	err := row.Scan(&id)
	errchk(err)
	return id
}

func db_task_update_state(id int, state string) {
	qf := `update "task" set "state" = '%s'`
	query := fmt.Sprintf(qf, state)
	_, err := db().Query(query)
	errchk(err)
}

func db_task_get_by_id(id int) task_t {
	var task task_t
	qf := `select * from "task" where "id" = %d`
	query := fmt.Sprintf(qf, id)
	row := db().QueryRow(query)
	err := row.Scan(&task.id, &task.parent, &task.uid, &task.state,
		&task.name, &task.args, &task.created, &task.updated)
	errchk(err)
	return task
}
