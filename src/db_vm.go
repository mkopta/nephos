package main

import "fmt"

func db_vm_get_all() vms_t {
	rows, err := db().Query(`select * from "vm"`)
	errchk(err)
	vms := make(vms_t, 0)
	for rows.Next() {
		vm := new(vm_t)
		err = rows.Scan(&vm.id, &vm.uid, &vm.name, &vm.cpu,
			&vm.ram)
		errchk(err)
		vms = append(vms, vm)
	}
	err = rows.Err()
	errchk(err)
	return vms
}

func db_vm_get_by_id(id int) vm_t {
	var vm vm_t
	qf := `select * from "vm" where "id" = %d`
	query := fmt.Sprintf(qf, id)
	row := db().QueryRow(query)
	err := row.Scan(&vm.id, &vm.uid, &vm.name, &vm.cpu, &vm.ram)
	errchk(err)
	return vm
}

func db_vm_create(uid int, name string, cpu, ram int) int {
	var id int
	qf := `insert into "vm"
		("uid", "name", "cpu", "ram")
		values (%d, '%s', %d, %d)
		returning "id"`
	query := fmt.Sprintf(qf, uid, name, cpu, ram)
	row := db().QueryRow(query)
	err := row.Scan(&id)
	errchk(err)
	return id
}

func db_vm_delete(id int) {
	query_fmt := `delete from "vm" where "id" = %d`
	query := fmt.Sprintf(query_fmt, id)
	_, err := db().Query(query)
	errchk(err)
}
