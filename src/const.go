package main

const (
	TASK_STATE_META = "meta"
	TASK_STATE_WAIT = "wait"
	TASK_STATE_SUCC = "succ"
	TASK_STATE_FAIL = "fail"
)
